<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\Status;
use App\Entity\User;
use App\Entity\UserBook;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory as Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker::create('fr_FR');

        // Créate of 10 Autors
        $authors = [];

        for ($i = 0; $i < 10; $i++) {
            $author = new Author();
            $author->setName($faker->name);
            $manager->persist($author);
            $authors[] = $author;
        }

        // Create 10 publisher
        $publishers = [];
        for ($i = 0; $i < 10; $i++) {
            $publisher = new Publisher();
            $publisher->setName($faker->company);
            $manager->persist($publisher);
            $publishers[] = $publisher;
        }

        // Create Status
        $status = [];

        foreach (['to read', 'reading', 'read'] as  $value) {
            $oneStatus = new Status();
            $oneStatus->setName($value);
            $manager->persist($oneStatus);
            $status[] = $oneStatus;
        }

        // Create 100 Books

        $books =  [];
        for ($i = 0; $i < 100; $i++) {
            $book = new Book();
            $book
                ->setGoogleBooksId($faker->uuid)
                ->setTitle($faker->sentence)
                ->setSubtitle($faker->sentence)
                ->setPublishDate($faker->dateTime)
                ->setDescription($faker->text)
                ->setIsbn13($faker->isbn13)
                ->setIsbn10($faker->isbn10)
                ->setPageCount($faker->numberBetween(100, 1000))
                ->setTrumbnail($faker->imageUrl(200, 300))
                ->setSmallTrumbnail($faker->imageUrl(100, 150))
                ->setAuthors($faker->randomElement($authors))
                ->setPublishers($faker->randomElement($publishers));
            $manager->persist($book);

            $books[] = $book;
        }
        // Create 10 User
        $users = [];
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user
                ->setEmail(($faker->email))
                ->setPassword($faker->password)
                ->setPseudo($faker->userName);
            $manager->persist($user);
            $users[] = $user;
        }

        //Create 10 UserBook by user
        foreach ($users as $user) {
            for ($i = 0; $i < 10; $i++) {
                $userBook = new UserBook();
                $userBook
                    ->setReader($user)
                    ->setRating($faker->numberBetween(1, 5))
                    ->setStatus($faker->randomElement($status))
                    ->setComment($faker->text)
                    ->setBook($faker->randomElement($books))
                    ->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime))
                    ->setUpdateAd(\DateTimeImmutable::createFromMutable($faker->dateTime));


                $manager->persist($userBook);

            }
        }

        $manager->flush();
    }
    }

